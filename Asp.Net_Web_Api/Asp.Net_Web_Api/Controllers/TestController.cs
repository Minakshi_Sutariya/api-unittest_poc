﻿using Asp.Net_Web_Api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;

namespace Asp.Net_Web_Api.Controllers
{
    public class TestController : ApiController
    {
        private User[] users = new User[]

       {

        new User { id = 1, name = "Minakshi Sutariya", email = "Minakshi@mail.com", phone = "12345678"},

        new User { id = 2, name = "Rahul Jadav", email = "Rahul@mail.com", phone = "87665677"},

        new User { id = 3, name = "Venu patel", email = "Venu@mail.com", phone = "56687989"},

        new User { id = 4, name = "Nilam Sutariya", email = "Nilam@mail.com", phone = "456675677"},

        new User { id = 5, name = "Twesha patel", email = "Twesha@mail.com", phone = "69879890"}

       };

        [HttpGet]
        public JsonResult<string> GetName()
        {
            return Json("Minakshi Sutariya");
        }
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return users;

        }
        /// <summary>
        /// Get User by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var usr = users.FirstOrDefault((p) => p.id == id);

            if (usr == null)

            {
                return NotFound();
            }

            return Ok(usr);

        }

    }
}
