using System.Web.Http;
using WebActivatorEx;
using Asp.Net_Web_Api;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Asp.Net_Web_Api
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "Asp.Net_Web_Api");
                    })
                .EnableSwaggerUi();
        }
    }
}
