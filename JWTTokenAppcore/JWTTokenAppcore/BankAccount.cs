﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWTTokenAppcore
{
    public class BankAccount : Icalculator
    {
        public double balance { get; set; }
        public BankAccount(double bal)
        {
            this.balance = bal;
        }
        public void Credit(double amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("amount is  not less than Zero");
            }
            balance += amount;
        }
        public void Debit(double amount)
        {
            if (amount > balance)
            {
                throw new ArgumentOutOfRangeException("amount is not Greater than Your Balance");
            }

            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("amount is not less than Zero");
            }

            balance -= amount;
        }

        public decimal Add(decimal num1, decimal num2)
        {
            return num1 + num2;
        }
    }
}
