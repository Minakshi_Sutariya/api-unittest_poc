using JWTTokenAppcore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BankTestP
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Credit Test Method
        /// </summary>
        [TestMethod]
        public void CreditBalance()
        {
            //Arrange
            double bal = 200;
            double Credit = 5;
            double expected = 205;
            BankAccount bankAccount = new BankAccount(bal);

            //Act
            bankAccount.Credit(Credit);
            double actual = bankAccount.balance;
            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Debit Test Method
        /// </summary>
        [TestMethod]
        public void DebitBalance()
        {
            //Arrange
            double bal = 200;
            double debit = 15;
            double expected = 185;
            BankAccount bankAccount = new BankAccount(bal);

            //Act
            bankAccount.Debit(debit);
            double actual = bankAccount.balance;
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void AddTest()
        {
            var calculator = new Mock<Icalculator>();
            calculator.Setup(a => a.Add(3, 3)).Returns(6);
            Assert.AreEqual(6, calculator.Object.Add(3, 3));
        }
    }
}
